## EVENT LOOP        
Browser has Javascript Engine. Javascript Engine has runtime model based on an EVENT LOOP which is responsible for executing the       
code and executing the Queed Sub-tasks waiting in the *CallBack Quee* and *Micro-Task Quee*            
          
### CallStack                     
CallStack is a mechanism for an Interpreter like the Javascript Interpreter in the Web Browser to keep track of its Place that         
calls multiple Functions..           
Every line in the Javascript Code executes in the CallStack only....    
Whenever the file of code enters the CallStack it executes immediately           
                
### Queue                  
Javascript runtime has a Quee contains list of CallBack functions ready to execute...              
Two types of Queue's are There                         
1. CallBack Queue         
2. Micro-Task Queue                           
                    
##### callBack Queue           
callBack from Web API's like setTimeOut, Event Listeners wait in the CallBack Queue         
##### Micro-Task Queue         
All the promise functions wait in the Micro-Task Queue       
              
Higher Priority given more to the Micro-Task Queue than CallBack- Queue           
               
### Web API's         
In between the Javascript code we can use the Web API's provided by the Browser. There are large number of Web API's available.       
Below are the most common used and known Web API's...          
1. setTimeOut()        
2. DOM API's          
3. fetch()         
4. localStorage       
5. console.log      
         
#### Code:         
     1. console.log("start")                   
     2. setTimeOut(function callBack(){                          
            console.log("callBack executed")                        
        }, 5000)              
     3. console.log("End")                
#### Order of Operations:            
     When the whole File of Code Enters the CallStack, It creates Global Execution Context (GEC)        
     and we all Know that Javascript is Synchronous and Single Threaded Language          
     1. In the first line It uses console.log() Web API and executes the line immediately       
     2. In second line setTimeOut Web API calls and registers in the Browser's Web API Environment and starts         
        the timer       
     3. while running the timer, It comes to the third line and consoles the End in the console using console Web API                
     4. After finishing the timer the Code Enters the CallBack Quee and wait for its turn And Event Loop allows the code     
        to CallStack for Execution          
#### code:           
     1. console.log("start")              
     2. setTimeOut(function cbT(){             
             console.log("setTimeOut")       
        }, 5000)                 
     3. fetch("https://api.netFlix.com")                       
        .then(function cbF(){            
              console.log("cB netFlix)           
         });           
     4. console.log("End")           
#### Order of Operations:           
      1. In the first line It uses console.log() Web API and executes the line immediately       
      2. In second line setTimeOut Web API calls and registers in the Browser's Web API Environment and starts         
         the timer          
      3. In the third line fetch web API calls and stores Browser's WEB API Environment        
      4. in the forth line it consoles the "End" with  console WEB API           
      5. If fetch Operation completed in less time than setTimeOut          
      6. callBack from fetch waits in Micro-Task Quee and callBack from setTimeOut waits in callBack Queue         
      7. callBack's from Micro-Task Queue Event Loop allows to CallStack than callBack from callBack Queue
